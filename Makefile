SOURCE=$(wildcard *.tex)
TARGET=$(SOURCE:.tex=.pdf)

all: $(TARGET)

$(TARGET): $(SOURCE)
	pdflatex $(SOURCE)
	bibtex $(basename $(SOURCE))
	pdflatex $(SOURCE)
	pdflatex $(SOURCE)
#	(okular $(TARGET) >/dev/null 2>&1 &)

clean:
	rm -f $(TARGET) *.log *.aux *.idx *.toc *.glo *.lof *.lot *.xdy *.out *.bbl *.blg *.4ct *.4tc *.dvi *.xref *.tmp *.html *.css *.idv *.lg *.png *.acn *.ist *.pyg

.PHONY: clean
